//
//  CreateNoteViewController.swift
//  IOS-Noted
//
//  Created by Nheng Vanchhay on 4/12/20.
//

import UIKit

protocol NoteDelegate {
    func getNoteData(title: String, desNote: String)
}

protocol UpdateNoteDelegate {
    func editNoteData(title: String, desNote: String)
}

class CreateNoteViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var titleNote: UITextView!
    @IBOutlet weak var desNote: UITextView!
    
    var getTitle: String?
    var getDes: String?
    var delegate: NoteDelegate?
    var updateDelegate: UpdateNoteDelegate?
    
    var takePhotoLang: String = ""
    var chooseImageLang: String = ""
    var drawingLang: String = ""
    var recordingLang: String = ""
    var checkBoxLang: String = ""
    var cancelLang: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (getTitle != nil) && (titleNote != nil){
            
            titleNote.text = getTitle
            desNote.text = getDes
        }else {
            titleNote.text = "Title here..."
            titleNote.textColor = .gray
            desNote.text = "Text here..."
            desNote.textColor = .gray
        }
        titleNote.delegate = self
        desNote.delegate = self
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        guard let title = titleNote.text else {
            return
        }
        guard let desNote = self.desNote.text else {
            return
        }
        delegate?.getNoteData(title: title, desNote: desNote)
        updateDelegate?.editNoteData(title: title, desNote: desNote)
    }
   
    @IBAction func pressAlert(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: self.takePhotoLang, style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: self.chooseImageLang, style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: self.drawingLang, style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: self.recordingLang, style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: self.checkBoxLang, style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: self.cancelLang, style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
extension CreateNoteViewController: SentNoteDelegate, SwiftLanguage{
    func setlang(titleNote: String, takePhotoLang: String, chooseImageLang: String, drawingLang: String, recordingLang: String, checkBoxLang: String, cancelLang: String) {
        self.navigationItem.title = titleNote
        self.takePhotoLang = takePhotoLang
        self.chooseImageLang = chooseImageLang
        self.drawingLang = drawingLang
        self.recordingLang = recordingLang
        self.checkBoxLang = checkBoxLang
        self.cancelLang = cancelLang
    }
    func sentNoteData(title: String, desNote: String) {
        self.getTitle = title
        self.getDes = desNote
    }
}

