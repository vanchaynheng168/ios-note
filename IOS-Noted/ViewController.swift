//
//  ViewController.swift
//  IOS-Noted
//
//  Created by Nheng Vanchhay on 4/12/20.
//

import UIKit
protocol SentNoteDelegate {
    func sentNoteData(title: String, desNote: String)
}
protocol SwiftLanguage {
    func setlang(titleNote: String, takePhotoLang: String, chooseImageLang: String, drawingLang: String, recordingLang: String, checkBoxLang: String, cancelLang: String)
}

class NotesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var descriptionNote: UILabel!
    @IBOutlet weak var titleNote: UILabel!
    
}

//List Note
class ViewController: UIViewController, NoteDelegate, UpdateNoteDelegate {
    
    var indexPathSelectedEdit: Int?
    var items: [Note] = []
    var sentDelegate: SentNoteDelegate?
    var langDeletgate: SwiftLanguage?
    let contact = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    var desDeleteLang: String = "desDelete".localizedString()
    var deleteLang: String = "delete".localizedString()
    var cancelLang: String = "cancel".localizedString()
    var backLang: String = "titleNote".localizedString()
    var titleCreateNote: String = "titleCreateNote".localizedString()
    
    var takePhoto: String = "takePhoto".localizedString()
    var chooseImage: String = "chooseImage".localizedString()
    var drawing: String = "drawing".localizedString()
    var recording:String = "recording".localizedString()
    var checkboxs: String = "checkboxs".localizedString()
    
    @IBOutlet weak var testLable: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var addTextNote: UIBarButtonItem!
    
   
    let longPressGesture = UILongPressGestureRecognizer()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "titleNote".localizedString()
        self.addTextNote.title = "createNote".localizedString()
        fetchData()
        self.view.addGestureRecognizer(longPressGesture)
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(true)
//        fetchData()
//    }

    func setLanguage(lang: String) {
        AppService.shared.choose(language: lang)
        self.navigationItem.title = "titleNote".localizedString()
        self.addTextNote.title = "createNote".localizedString()
        self.backLang = "titleNote".localizedString()
        self.titleCreateNote = "titleCreateNote".localizedString()
        self.desDeleteLang = "desDelete".localizedString()
        self.deleteLang = "delete".localizedString()
        self.cancelLang = "cancel".localizedString()
        
        self.takePhoto = "takePhoto".localizedString()
        self.chooseImage = "chooseImage".localizedString()
        self.drawing = "drawing".localizedString()
        self.recording = "recording".localizedString()
        self.checkboxs = "checkboxs".localizedString()
    }
    
    @IBAction func changeLaguage(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Khmer", style: .default, handler: {_ in
            self.setLanguage(lang: language.khmer.rawValue)
        }))
        alert.addAction(UIAlertAction(title: "English", style: .default, handler: { _ in
            self.setLanguage(lang: language.english.rawValue)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func fetchData() {
        do{
            self.items = try contact.fetch(Note.fetchRequest())
            self.collectionView.reloadData()
        }catch{
            print("error")
        }
    }
    func saveItem(title: String, detail: String){
        if (title != "Title here..." && detail != "Text here...") == true {
            let contact = Note(context: self.contact)
            contact.title = title
            contact.noteDetail = detail
            try? self.contact.save()
            fetchData()
            
        }else{
            fetchData()
        }
        
    }
    func getNoteData(title: String, desNote: String) {
        saveItem(title: title, detail: desNote)
    }
 
    @IBAction func addNotePress(_ sender: Any) {
        let noteVC = storyboard!.instantiateViewController(withIdentifier: "noteViewController") as! CreateNoteViewController
        noteVC.delegate = self
        self.langDeletgate = noteVC
        self.langDeletgate?.setlang(titleNote: titleCreateNote, takePhotoLang: takePhoto, chooseImageLang: chooseImage, drawingLang: drawing, recordingLang: recording, checkBoxLang: checkboxs, cancelLang: cancelLang)
        self.navigationController!.pushViewController(noteVC, animated: true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: backLang, style: .plain, target: self, action: nil)
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! NotesCollectionViewCell
        cell.titleNote?.text = items[indexPath.row].title
        cell.descriptionNote?.text = items[indexPath.row].noteDetail
        
        let longPressGesture = CustomTapGestureRecognizer(target: self,
                                                    action: #selector(deleteTapSelector(sender:)))
        cell.addGestureRecognizer(longPressGesture)
        return cell
    }

    //delete note
    @objc
      func deleteTapSelector(sender: CustomTapGestureRecognizer) {
        let alert = UIAlertController(title: nil, message: desDeleteLang, preferredStyle: .actionSheet)
        if sender.state == UIGestureRecognizer.State.began {
                let touchPoint = sender.location(in: collectionView)
                if let index = collectionView.indexPathForItem(at: touchPoint) {
                    alert.addAction(UIAlertAction(title: deleteLang, style: .destructive, handler: {_ in
                        let noteDelete = self.items[index.row]
                        self.contact.delete(noteDelete)
                       try? self.contact.save()
                       self.fetchData()
                    }))
                }
            }
        alert.addAction(UIAlertAction(title: cancelLang, style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
      }
    
    //edit note
    func editNoteData(title: String, desNote: String) {
        let noteSelected = self.items[indexPathSelectedEdit!]
        //update note
        print(title, desNote)
        noteSelected.title = title
        noteSelected.noteDetail = desNote
        try? self.contact.save()
        self.fetchData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        indexPathSelectedEdit = indexPath.row
        let noteSelected = self.items[indexPath.row]
        let noteVC = storyboard!.instantiateViewController(withIdentifier: "noteViewController") as! CreateNoteViewController
        self.sentDelegate = noteVC
        noteVC.updateDelegate = self
        self.sentDelegate?.sentNoteData(title: noteSelected.title!, desNote: noteSelected.noteDetail!)
        
        self.navigationController!.pushViewController(noteVC, animated: true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Notes", style: .plain, target: self, action: nil)
    }
}

class CustomTapGestureRecognizer: UILongPressGestureRecognizer {
    var indexPath: Int?
}

extension String {
    func localizedString()->String {
        let path = Bundle.main.path(forResource: AppService.shared.language, ofType: "lproj")!
        let bundle = Bundle(path: path)!
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: self, comment: self)
    }
}

